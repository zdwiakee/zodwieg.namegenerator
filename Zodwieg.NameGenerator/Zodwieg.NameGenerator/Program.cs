﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zodwieg.NameGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            const int count = 50;
            const string path = "out.txt";

            var generator = new NameGenerator.Core.NameGenerator()
            {
                FrequentVowels = "aeiou",
                InfrequentVowels = "yáæéíóöúý",
                FrequentConsonants = "bdfghjklmnprstv",
                InfrequentConsonants = "ðþx"
            };
            generator.Initialize();
            //File.WriteAllLines(path,
            //    Enumerable.Range(0, count)
            //        .Select(i => generator.GenerateNext())
            //        .Select(s => String.Format("{0}{1}", Char.ToUpper(s[0]), s.Substring(1))));
            for (int i = 1; i < count; i++)
            {
                Console.WriteLine("{0} {1}",generator.GetFirstName(), generator.GetLastName());
            }
            Console.Read();
        }
    }
}
