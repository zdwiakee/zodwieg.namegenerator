﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zodwieg.NameGenerator.Core
{
    internal struct Probability<T>
    {
        public T Value { get; private set; }
        public double ProbabilityValue { get; private set; }

        public Probability(T value, double probability) : this()
        {
            Value = value;
            ProbabilityValue = probability;
        }

        public override string ToString()
        {
            return String.Format("{0}: {1}", Value, ProbabilityValue);
        }
    }
}
