﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zodwieg.NameGenerator.Core
{
    public class NameGenerator
    {

        //Nearly all of constants below should be generated while generating language,
        //because they are evidently main language properties.
        private const double InflectionProbability = 0.7;
        private const double InfrequentCharProbability = 0.22;

        private const bool UsingAristocraticPrefixes = true;
        private const bool UsingSeparateLastNameFromFirstNameInflections = true;
        private const bool UsingHyphen = true;

        private const int CharPairCount = 10;
        private const int FirstNameInflectionCount = 10;
        private const int LastNameInflectionCount = 10;
        private const int LastNameFromFirstNameInflectionCount = 1;
        private const int CityNameInflectionCount = 10;
        private const int AristocraticPrefixCount = 2;
        private const int FirstNameCount = 50;
        private const int CommonLastNameCount = 50;
        private const int CityAdjectiveCount = 20;

        private const double FirstSyllablePartProbability = 0.85;
        private const double SecondSyllablePartProbability = 1;
        private const double ThirdSyllablePartProbability = 0.15;
        private const double PairProbability = 0.07;
        private const double LastNameFromFirstNameProbability = 0.33;
        private const double FirstNameEqualsLastNameProbability = 0.33;
        private const double CommonLastNameProbability = 0.5;
        private const double DoubleFirstNameProbability = 0.1;
        private const double DoubleLastNameProbability = 0.05;
        private const double DoubleCityNameProbability = 0.05;
        private const double CityAdjectiveProbability = 0.05;
        private const double AristocraticPrefixProbability = 0.1;

        private readonly Random _random;

        private List<Probability<char>> _vowels;
        private List<Probability<char>> _consonants;
        private List<Probability<string>> _vowelPairs;
        private List<Probability<string>> _consonantPairs;
        private List<Probability<string>> _firstNameInflections;
        private List<Probability<string>> _lastNameInflections;
        private List<Probability<string>> _lastNameFromFirstNameInflections;
        private List<Probability<string>> _cityNameInflections;
        private List<Probability<string>> _aristocraticPrefixes;
        private List<Probability<string>> _firstNames;
        private List<Probability<string>> _commonLastNames;
        private List<Probability<string>> _cityAdjectives;

        public IEnumerable<char> FrequentVowels { get; set; }
        public IEnumerable<char> InfrequentVowels { get; set; }
        public IEnumerable<char> FrequentConsonants { get; set; }
        public IEnumerable<char> InfrequentConsonants { get; set; }

        public NameGenerator()
        {
            _random = new Random();
        }

        public void Initialize()
        {
            GenerateCharProbabilities();
            GenerateCharPairs();
            _firstNameInflections = GenerateInflections(FirstNameInflectionCount);
            _lastNameInflections = GenerateInflections(LastNameInflectionCount);
            _cityNameInflections = GenerateInflections(CityNameInflectionCount);
            _aristocraticPrefixes = GenerateInflections(AristocraticPrefixCount);
            _lastNameFromFirstNameInflections = GenerateInflections(LastNameFromFirstNameInflectionCount);
            _firstNames = GenerateNames(_firstNameInflections, FirstNameCount);
            _commonLastNames = GenerateNames(_lastNameInflections, CommonLastNameCount);
            _cityAdjectives = GenerateNames(_cityNameInflections, CityAdjectiveCount);

        }

        public string GetCityName()
        {
            var result = new StringBuilder();
            int CityNamePartsCount = 0;
            string separator = "";
            if (UsingHyphen) separator = "-"; else separator = " ";
            if (_random.NextDouble() < DoubleCityNameProbability) CityNamePartsCount = 1;
            if (_random.NextDouble() < CityAdjectiveProbability)
            {
                string s = SelectRandom(_cityAdjectives);
                result.Append(String.Format("{0}{1}{2}", Char.ToUpper(s[0]), s.Substring(1), " "));
            }
            for (int i = 0; i <= CityNamePartsCount; i++)
            {
                if (i != 0) result.Append(separator);
                string s = GenerateNext(_cityNameInflections);
                result.Append(String.Format("{0}{1}", Char.ToUpper(s[0]), s.Substring(1)));
            }
            return result.ToString();
        }

        public string GetFirstName()
        {
            var result = new StringBuilder();
            int FirstNamePartsCount = 0;
            string separator = "";
            if (UsingHyphen) separator = "-"; else separator = " ";
            if (_random.NextDouble() < DoubleFirstNameProbability) FirstNamePartsCount = 1;
            for (int i = 0; i <= FirstNamePartsCount; i++)
            {
                if (i != 0) result.Append(separator);
                string s = SelectRandom(_firstNames);
                result.Append(String.Format("{0}{1}", Char.ToUpper(s[0]), s.Substring(1)));
            }
            return (result.ToString());
        }

        public string GetLastName()
        {
            int LastNamePartsCount = 0;
            string separator = "";
            if (UsingHyphen) separator = "-"; else separator = " ";
            if (_random.NextDouble() < DoubleLastNameProbability) LastNamePartsCount = 1;
            var result = new StringBuilder();
            if (UsingAristocraticPrefixes)
            {
                if (_random.NextDouble() < AristocraticPrefixProbability)
                {
                    result.Append(SelectRandom(_aristocraticPrefixes));
                    result.Append(" ");
                }
            }
            string s = "";
            for (int i = 0; i <= LastNamePartsCount; i++)
            {
                if (i == 1) result.Append(separator);
                if (_random.NextDouble() < LastNameFromFirstNameProbability)
                    if (_random.NextDouble() < FirstNameEqualsLastNameProbability)
                        s = GetFirstName();
                    else
                        if (UsingSeparateLastNameFromFirstNameInflections)
                            s = GetFirstName() + SelectRandom(_lastNameFromFirstNameInflections);
                        else
                            s = GetFirstName() + SelectRandom(_lastNameInflections);
                else
                    if (_random.NextDouble() < CommonLastNameProbability)
                        s = SelectRandom(_commonLastNames);
                    else
                        s = GenerateNext(_lastNameInflections);
                result.Append(String.Format("{0}{1}", char.ToUpper(s[0]), s.Substring(1)));
            }
            return result.ToString();
        }

        private List<Probability<string>> GenerateNames(IList<Probability<string>> inflectionList, int NameCount)
        {
            var result = new List<Probability<string>>();
            for (int i = 0; i < NameCount; i++)
                result.Add(new Probability<string>(GenerateNext(inflectionList), _random.NextDouble()));
            return result;
        }

        private string GenerateNext(IList<Probability<string>> inflectionList)
        {
            if (_consonants == null)
                throw new InvalidOperationException("Generator is not initialized.");
            int wordLengthInSyllables = _random.Next(1, 4);
            var syllables = Enumerable.Range(0, wordLengthInSyllables).Select(i => GenerateSyllable()).ToList();
            if (_random.NextDouble() <= InflectionProbability)
                syllables.Add(SelectRandom(inflectionList));
            return String.Join(String.Empty, syllables);
        }

        private void GenerateCharProbabilities()
        {
            _vowels = GenerateCharProbabilities(FrequentVowels, InfrequentVowels);
            _consonants = GenerateCharProbabilities(FrequentConsonants, InfrequentConsonants);
        }

        private List<Probability<char>> GenerateCharProbabilities(IEnumerable<char> frequent, IEnumerable<char> infrequent)
        {
            var result = new List<Probability<char>>();
            result.AddRange(frequent.Select(c => new Probability<char>(c, _random.NextDouble())));
            foreach (var c in infrequent)
            {
                double probability = _random.NextDouble();
                if (probability <= InfrequentCharProbability)
                    result.Add(new Probability<char>(c, probability));
            }
            return result;
        }

        private void GenerateCharPairs()
        {
            _vowelPairs = GenerateCharPairs(_vowels);
            _consonantPairs = GenerateCharPairs(_consonants);
        }

        private List<Probability<string>> GenerateCharPairs(IList<Probability<char>> charProbabilities)
        {
            return Enumerable.Range(0, CharPairCount)
                .Select(i => new char[] { SelectRandom(charProbabilities), SelectRandom(charProbabilities) })
                .Select(chars => new Probability<string>(new String(chars), _random.NextDouble()))
                .ToList();
        }

        private List<Probability<string>> GenerateInflections(int inflectionCount)
        {
            return Enumerable.Range(0, inflectionCount)
                .Select(i => new Probability<string>(GenerateSyllable(), _random.NextDouble()))
                .ToList();
        }

        private string GenerateSyllable()
        {
            var sb = new StringBuilder();
            sb.Append(GenerateSyllablePart(FirstSyllablePartProbability, _consonants, _consonantPairs));
            sb.Append(GenerateSyllablePart(SecondSyllablePartProbability, _vowels, _vowelPairs));
            sb.Append(GenerateSyllablePart(ThirdSyllablePartProbability, _consonants, _consonantPairs));
            return sb.ToString();
        }

        private string GenerateSyllablePart(double threshold, 
            IList<Probability<char>> charProbabilities, IList<Probability<string>> charPairProbabilities)
        {
            var r = _random.NextDouble();
            if (r > threshold)
                return String.Empty;
            r = _random.NextDouble();
            if (r <= PairProbability)
                return SelectRandom(charPairProbabilities);
            else
                return SelectRandom(charProbabilities).ToString();
        }

        private T SelectRandom<T>(IList<Probability<T>> collection)
        {
            var threshold = collection.Max(p => p.ProbabilityValue);
            double r = _random.NextDouble() * threshold;
            int i;
            do
            {
                i = _random.Next(collection.Count);
            } while (collection[i].ProbabilityValue <= r);
            return collection[i].Value;
        }
    }
}
